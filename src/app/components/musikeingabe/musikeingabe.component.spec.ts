import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusikeingabeComponent } from './musikeingabe.component';

describe('MusikeingabeComponent', () => {
  let component: MusikeingabeComponent;
  let fixture: ComponentFixture<MusikeingabeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusikeingabeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusikeingabeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
