import { Component, OnInit } from '@angular/core';
import {Songstore} from '../../common/songstore';

@Component({
  selector: 'app-musikeingabe',
  templateUrl: './musikeingabe.component.html',
  styleUrls: ['./musikeingabe.component.css']
})
export class MusikeingabeComponent implements OnInit {

  interpret = ''
  titel = ''
  constructor() { }

  ngOnInit(): void {
  }

  public insertMusic() {
    Songstore.songs.push({
      titel: this.titel,
      interpret: this.interpret
    });
  }

}
