import { Component, OnInit } from '@angular/core';
import { Songstore } from '../../common/songstore';

@Component({
  selector: 'app-musikliste',
  templateUrl: './musikliste.component.html',
  styleUrls: ['./musikliste.component.css']
})
export class MusiklisteComponent implements OnInit {

  liedliste = Songstore.songs;

  displayedColumns: string[] = ['interpret'];
  spalten: string[] = ['interpret', 'titel'];

  constructor() { }

  ngOnInit(): void {
  }

}
