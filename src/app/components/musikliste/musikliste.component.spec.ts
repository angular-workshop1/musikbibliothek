import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusiklisteComponent } from './musikliste.component';

describe('MusiklisteComponent', () => {
  let component: MusiklisteComponent;
  let fixture: ComponentFixture<MusiklisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusiklisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusiklisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
